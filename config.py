from PIL import Image


class Config:
    location = {
        "Branch name": (79, 316),
        "Cert No": (2042, 413),
        "Date": (2180, 456),
        "Account No": (271, 1212),
        "Account Name(s)/ Client": (530, 1212),
        "Account Balance": (1059, 1212),
        "Account Opening day (MM/DD/YYYY)": (1268, 1212),
        "Average daily Balance over the last 6 months": (1687, 1212),

    }
    scale = 0.9
    img_form_1 = Image.open('docs/Blank Bank.png')


cfg = Config()
