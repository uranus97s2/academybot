import cv2
import numpy as np
from PIL import Image, ImageDraw, ImageFont

from config import cfg


def click_event(img, event, x, y, flags, params):
    # checking for left mouse clicks
    if event == cv2.EVENT_LBUTTONDOWN:
        # displaying the coordinates
        # on the Shell
        print(x, ' ', y)

        # displaying the coordinates
        # on the image window
        font = cv2.FONT_HERSHEY_SIMPLEX
        cv2.putText(img, str(x) + ',' +
                    str(y), (x, y), font,
                    1, (255, 0, 0), 2)
        cv2.imshow('image', img)

    # checking for right mouse clicks
    if event == cv2.EVENT_RBUTTONDOWN:
        # displaying the coordinates
        # on the Shell
        print(x, ' ', y)

        # displaying the coordinates
        # on the image window
        font = cv2.FONT_HERSHEY_SIMPLEX
        b = img[y, x, 0]
        g = img[y, x, 1]
        r = img[y, x, 2]
        cv2.putText(img, str(b) + ',' +
                    str(g) + ',' + str(r),
                    (x, y), font, 1,
                    (255, 255, 0), 2)
        cv2.imshow('image', img)


def check_point(img):
    # displaying the image
    cv2.imshow('image', img)
    # setting mouse handler for the image
    # and calling the click_event() function
    cv2.setMouseCallback('image', click_event)
    # wait for a key to be pressed to exit
    cv2.waitKey(0)
    # close the window
    cv2.destroyAllWindows()


def draw_image(pil_img, text, position, text_rgb_color=(0, 0, 0), text_size=31, font="fonts/times new roman bold.ttf"):
    draw = ImageDraw.Draw(pil_img)
    font_text = ImageFont.truetype(font=font, size=text_size, encoding="utf-8")
    draw.text(position, text, text_rgb_color, font=font_text)
    return pil_img


def end_line(account_name, position):
    max_len = 20
    pad = 30
    q = []
    p = [position]
    for i in range(0, len(account_name), max_len):
        q += [account_name[i: i + max_len]]
    p *= len(q)
    for i in range(1, len(q)):
        p[i] = (p[i][0], p[i - 1][1] + pad)
    return zip(q, p)


def generate_doc(config_form):
    name = 'Account Name(s)/ Client'
    blank_img = cfg.img_form_1.copy()
    for n, p in end_line(config_form[name], cfg.location[name]):
        draw_image(blank_img, n, (p[0], p[1] - 25))

    new_config_from = config_form.copy()
    del new_config_from[name]

    for k, v in new_config_from.items():
        draw_image(blank_img, v, (cfg.location[k][0], cfg.location[k][1] - 27))
    #
    # cv2.imshow('im', cv2.resize(np.array(blank_img)[:, :, ::-1], (592 * 2, 852 * 2)))
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()
    return blank_img


def example():
    config_form = {
        "Branch name": "Bislig Branch",
        "Cert No": "453-2022-21-0219-2",
        "Date": "21/02/2022",
        "Account No": "360-9-21109424-7",
        "Account Name(s)/ Client": "Charmagne Pahayahay Cantillas",
        "Account Balance": "530,128.60",
        "Account Opening day (MM/DD/YYYY)": "05/04/2011",
        "Average daily Balance over the last 6 months": "120,260.61"
    }
    # reading the image
    img = cv2.imread('/home/duongpd/Downloads/Blank Bank.png')

    name = 'Account Name(s)/ Client'
    for n, p in end_line(config_form[name], cfg.location[name]):
        cv2.putText(img, n, p, cv2.FONT_HERSHEY_SIMPLEX, cfg.scale, (0, 0, 0), 2, cv2.LINE_AA)

    new_config_from = config_form.copy()
    del new_config_from[name]

    for k, v in new_config_from.items():
        if k == "Account No":
            cfg.scale = 0.7
        else:
            cfg.scale = 0.8
        cv2.putText(img, v, cfg.location[k], cv2.FONT_HERSHEY_SIMPLEX, cfg.scale, (0, 0, 0), 2, cv2.LINE_AA)

    cv2.imshow("im", cv2.resize(img, (595 * 2, 852 * 2)))
    cv2.waitKey(0)
    cv2.destroyAllWindows()


if __name__ == "__main__":
    config_form = {
        "Branch name": "Bislig Branch",
        "Cert No": "453-2022-21-0219-2",
        "Date": "21/02/2022",
        "Account No": "360-9-21109424-7",
        "Account Name(s)/ Client": "Charmagne Pahayahay Cantillas",
        "Account Balance": "530,128.60",
        "Account Opening day (MM/DD/YYYY)": "05/04/2011",
        "Average daily Balance over the last 6 months": "120,260.61"
    }
    # generate_doc(config_form)
    example()
