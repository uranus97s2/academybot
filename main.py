import os
import telebot
import uuid


from help_func import generate_doc

token = '5167127385:AAGvPiroZxNRa4hSnvjd4SvmZpizTmbGPvA'
bot = telebot.TeleBot(token)


@bot.message_handler(content_types=['text'])
def repeat_all_message(message):
    string = message.text

    try:
        config = eval(string)
        if isinstance(config, dict):
            image = generate_doc(config)
            image_temp = uuid.uuid4().hex
            domain = 'jpg'
            image_name = f'{image_temp}.{domain}'
            image.save(image_name)
            bot.send_photo(message.chat.id, photo=open(image_name, 'rb'))
            os.remove(image_name)
        else:
            bot.send_message(message.chat.id, 'Error! You must be true config format')

    except Exception as e:
        bot.send_message(message.chat.id, 'Error! Some thing went wrong')


if __name__ == '__main__':
    bot.polling(none_stop=True)
